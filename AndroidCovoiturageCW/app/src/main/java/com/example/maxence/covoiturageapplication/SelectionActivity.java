package com.example.maxence.covoiturageapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.Vector;


public class SelectionActivity extends AppCompatActivity {



    Vector<Trajet> trajets = new Vector<Trajet>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);


        Intent recuperationTrajets = getIntent(); // récupération des trajets des activités précédentes
        Enregistrement.chargerVecteurTrajetv(trajets,recuperationTrajets);





    }

    public void versrech(View view) {


        EditText etj = findViewById(R.id.editText); // déclaration des zones de textes pour les champs de recherche
        EditText eth = findViewById(R.id.editText2);
        EditText etm = findViewById(R.id.editText3);
        EditText etvd = findViewById(R.id.editText4);
        EditText etva = findViewById(R.id.editText5);

        String jour = etj.getText().toString();
        int h = Integer.parseInt(eth.getText().toString());
        int min = Integer.parseInt(etm.getText().toString());
        String villd = etvd.getText().toString();
        String villa = etva.getText().toString();




        int n =trajets.size();
        Vector<Integer> vect = new Vector<Integer>(); // ce vecteur va contenir les positions dans le vecteur trajets des trajets qui répondent aux exigences de la recherhce

        for(int i=0;i<n;i++){
            Trajet t = trajets.get(i);
            String depart = t.getDepart();
            String arrivee = t.getArrivee();
            int heure = t.getHeure();
            int minutes = t.getMinutes();
            String jourDeLaSemaine = t.getJour();

            if (jourDeLaSemaine.equals(jour) && heure==h && minutes==min && depart.equals(villd) && villa.equals(arrivee)){ // on teste la compatibilité du trajet avec la recherche
                vect.add(i);
            }


        }
        Intent InfoversRechercheSelec=new Intent();

        for (int j =0 ; j<trajets.size() ; j++) { // on passe en Extra le vecteur trajet
            //envoie du vecteur trajet pour le conserver
            Trajet sauvtrajet = trajets.get(j);

            InfoversRechercheSelec.putExtra("sauvegarde"+j, sauvtrajet);
        }



        InfoversRechercheSelec.putExtra("num_trajet_selec",vect);
        InfoversRechercheSelec.setClass(this, RechercheSelec.class);
        startActivity(InfoversRechercheSelec);






    }
}
