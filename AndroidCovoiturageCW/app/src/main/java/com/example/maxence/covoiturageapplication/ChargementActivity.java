package com.example.maxence.covoiturageapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Vector;

public class ChargementActivity extends AppCompatActivity {
    Vector<Trajet> trajets = new Vector<Trajet>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chargement);
        //TO DO enregistrer les trajets non pas dans un SharedPreferences qui est spécifique au smartphone mais dans une base de donnée accessible sur internet

        //création de trajet test pour l'afficher dès l'ouverture de l'application
        Trajet trajet0 = new Trajet("Dupont Thomas","Gif-surYvette","Paris",10,"Lundi",8,30,3,"Ceci est le trajet test","0651168712");

        //enregistrement du 1er trajet grace à la classe SharedPreferences
        SharedPreferences enreg = getSharedPreferences(String.valueOf(0), MODE_PRIVATE);
        SharedPreferences.Editor ed = enreg.edit();
        ed.putString("nom_prenom",trajet0.getNomPrenom());
        ed.putString("depart",trajet0.getDepart());
        ed.putString("arrivee",trajet0.getArrivee());
        ed.putInt("prix",trajet0.getPrix());
        ed.putString("jour",trajet0.getJour());
        ed.putInt("heure",trajet0.getHeure());
        ed.putInt("minute",trajet0.getMinutes());
        ed.putInt("nbplace",trajet0.getNbplaces());
        ed.putString("commentaire",trajet0.getCommentaire());
        ed.putString("num",trajet0.getNumeroTel());
        ed.putBoolean("existance",trajet0.getExistant());
        ed.apply();

        int i = 0;
        boolean sortie = false;//variable utiliser pour savoir quand sortir de la boucle de récupération de donnée

        while (sortie == false) {
            //boucle pour récupérer tous les trajets préalablement inversé (TO DO : partie à changée avec base de donnée)
            SharedPreferences sp = getSharedPreferences(String.valueOf(i), MODE_PRIVATE);
            i++;

            //récupération de toutes les données enregistrées
            String nom_prenom = sp.getString("nom_prenom","il faut sortir");
            String depart = sp.getString("depart","");
            String arrivee = sp.getString("arrivee","");
            int prix = sp.getInt("prix",0);
            String jour = sp.getString("jour","");
            int heure = sp.getInt("heure",0);
            int minute = sp.getInt("minute",0);
            int nbplace = sp.getInt("nbplace",0);
            String commentaire = sp.getString("commentaire","");
            String num = sp.getString("num","");
            boolean existance = sp.getBoolean("existance",true);

            //enregistrement dans un vecteur de trajet que l'on fera passer d'une activitée à l'autre
            Trajet trajet = new Trajet(nom_prenom, depart, arrivee, prix, jour, heure, minute, nbplace, commentaire, num);
            trajets.add(trajet);

            if(nom_prenom.equals("il faut sortir")){
                //si le prénom a pris la valeur par défaut, il n'y a plus de dossier à récupérer
                sortie = true;
                trajets.remove(i-1);
            }
        }



        //transfert des données à l'activité suivante
        Intent annonceVersRecherche = new Intent();
        for (int j =0 ; j<trajets.size() ; j++) {

            //transfert de trajet un à un car problème de transfert d'un vecteur entier (on ne sait pas pourquoi)
            Trajet sauvtrajet = trajets.get(j);
            annonceVersRecherche.setClass(this, RechercheActivity.class);
            annonceVersRecherche.putExtra("sauvegarde"+j, sauvtrajet);
        }

        startActivity(annonceVersRecherche);
        finish();

    }
}
