package com.example.maxence.covoiturageapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Vector;
import java.util.concurrent.ExecutionException;

public class RechercheActivity extends AppCompatActivity implements View.OnClickListener {

   Vector<Trajet> trajets = new Vector<Trajet>();
   Trajet trajet;
   LinearLayout lr;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recherche);

        lr = findViewById(R.id.scrolllayout); // déclaration du layout pour pouvoir le modifier depuis le code java

        //récupération du vecteur trajet
        Intent recuperationTrajets = getIntent();
        Enregistrement.chargerVecteurTrajetv(trajets,recuperationTrajets);


        if(vide(trajets)){
            //si aucun trajet n'a encore été enregistré
            TextView message = new TextView(this);
            message.setText("Aucun trajet disponible");
            lr.addView(message);
        }

        else{
            //affichage des trajets déja présent dans la "base de donnée"
            for(int i=0;i<trajets.size();i++){
                trajet = trajets.get(i);
                if(trajet.getExistant()) {
                    Button annonce = new Button(this); // chaque trajet est représenté par un bouton sur lequel on met une String pour décrire le trajet

                    String nom_prenom = trajet.getNomPrenom();
                    String depart = trajet.getDepart();
                    String arrivee = trajet.getArrivee();
                    int prix = trajet.getPrix();
                    String jour = trajet.getJour();
                    int heure = trajet.getHeure();
                    int minute = trajet.getMinutes();
                    int nbplace = trajet.getNbplaces();
                    String commentaire = trajet.getCommentaire();

                    annonce.setId(i);
                    annonce.setTag(i);
                    annonce.setText("Trajet de " + depart + " à " + arrivee + " les " + jour + " à " + heure + "h" + minute + " pour " + nbplace + " personne(s).\nPrix : " + prix + "\nConducteur : " + nom_prenom + "\nCommentaire : " + commentaire);
                    annonce.setOnClickListener(this);
                    lr.addView(annonce); // on implémente le bouton sur le layout

                    Button suppression = new Button(this);
                    suppression.setTag(i);
                    suppression.setText("Supprimer");
                    suppression.setOnClickListener(this);
                    lr.addView(suppression); // ajout d'un bouton supprimer
                }
            }

        }

    }

    private boolean vide(Vector<Trajet> trajets) { // méthode qui teste si il reste des places dans le trajet
        for(Trajet traj : trajets){
            if(traj.getExistant()){return false;}
        }
        return true;
    }

    public void clicknewtrajet(View view) { // méthode associée au bouton "créer trajet"
        //bouton plus qui permet d'ajouter un trajet
        Button boutonClique = (Button) view;
        Intent AjouterVersCreerTrajet = new Intent();

        for (int j =0 ; j<trajets.size() ; j++) {
            //envoie du vecteur trajets pour le conserver
            Trajet sauvtrajet = trajets.get(j);
            AjouterVersCreerTrajet.setClass(this, CreerTrajet.class);
            AjouterVersCreerTrajet.putExtra("sauvegarde"+j, sauvtrajet);
        }
        startActivity(AjouterVersCreerTrajet);
    }

    @Override
    public void onClick(View v) { // méthode associée à chaque bouton "trajet" pour pouvoir consulter plus précisément le trajet
        Button boutonClique = (Button) v;
        String string= (String) boutonClique.getText();
        int place = (int) boutonClique.getTag(); // le tag permet d'identifier facilement le trajet considéré

        if(string.contains("Supprimer")){ // partie de la méthode qui gère la suppression d'un trajet
            Trajet trajetASupprimer = trajets.get(place);
            trajetASupprimer.delete();
            Button boutonAnnonce = (Button) findViewById(place);
            boutonClique.setVisibility(View.INVISIBLE);
            boutonAnnonce.setVisibility(View.INVISIBLE);

            if(vide(trajets)) {
                //plus aucun trajet enregistré
                TextView message = new TextView(this);
                message.setText("Aucun trajet disponible");
                lr.addView(message);
            }

            SharedPreferences enreg = getSharedPreferences(String.valueOf(place), MODE_PRIVATE);
            SharedPreferences.Editor ed = enreg.edit();
            ed.putBoolean("existance",false);
            ed.apply();
        }

        else{
            Intent annonceVersReservation = new Intent();
            for (int j =0 ; j<trajets.size() ; j++) {
                //envoie du vecteur trajet pour le conserver
                Trajet sauvtrajet = trajets.get(j);
                annonceVersReservation.setClass(this, CreerTrajet.class);
                annonceVersReservation.putExtra("sauvegarde"+j, sauvtrajet);
                }

            //envoie du trajet qui a été sélectionné
            annonceVersReservation.setClass(this,ReservationActivity.class);
            annonceVersReservation.putExtra("num_trajet",place);
            startActivity(annonceVersReservation);
        }

    }

    public void effectuerrecherche(View view) {
        Intent annonceVersSelection = new Intent();
        for (int j =0 ; j<trajets.size() ; j++) {
            //envoie du vecteur trajet pour le conserver
            Trajet sauvtrajet = trajets.get(j);
            annonceVersSelection.setClass(this, SelectionActivity.class);
            annonceVersSelection.putExtra("sauvegarde"+j, sauvtrajet);
        }
        startActivity(annonceVersSelection);


    }
}
