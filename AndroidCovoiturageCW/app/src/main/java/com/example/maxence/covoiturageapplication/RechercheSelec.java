package com.example.maxence.covoiturageapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Vector;

public class RechercheSelec extends AppCompatActivity {

    Vector<Trajet> trajets = new Vector<Trajet>();
    Vector<Trajet> trajets_selec = new Vector<Trajet>();
    ArrayList<Integer> num_trajets_selec = new ArrayList<>();
    Trajet trajet;
    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recherche_selec);

        LinearLayout ll = findViewById(R.id.layoutrechercheselec); // Déclaration du layout dans le code Java pour créer les zones de textes par la suite

        Intent recuperationTrajets = getIntent();
        Enregistrement.chargerVecteurTrajetv(trajets,recuperationTrajets); // On importe le vecteur "trajets" contenant tous les trajets encore disponibles

        num_trajets_selec = (ArrayList<Integer>) recuperationTrajets.getSerializableExtra("num_trajet_selec"); // vecteur qui va contenir l'id des trajets qui répondent à la demande de la recherche
        if (num_trajets_selec.size() != 0 && trajets.get(num_trajets_selec.get(0)).getNbplaces() != 0) { // on teste si il existe bien des trajets qui répondent à ces critères et si il reste encore de la place
            try {
                for (int i = 0; i < num_trajets_selec.size(); i++) {
                    trajets_selec.add(trajets.get(num_trajets_selec.get(i))); // on ajoute alors les "bons" trajets à un nouveau vecteur de trajets
                }

                for (int i = 0; i < trajets_selec.size(); i++) { // on affiche alors les trajets voulus à l'aide de boutons
                    trajet = trajets_selec.get(i);
                    if (trajet.getExistant()) {
                        Button annonce = findViewById(R.id.buttonfind);

                        String nom_prenom = trajet.getNomPrenom(); // on importe les attributs du trajet considréré
                        String depart = trajet.getDepart();
                        String arrivee = trajet.getArrivee();
                        int prix = trajet.getPrix();
                        String jour = trajet.getJour();
                        int heure = trajet.getHeure();
                        int minute = trajet.getMinutes();
                        int nbplace = trajet.getNbplaces();
                        String commentaire = trajet.getCommentaire();

                        annonce.setId(i);
                        annonce.setTag(i);
                        annonce.setText("Trajet de " + depart + " à " + arrivee + " les " + jour + " à " + heure + "h" + minute + " pour " + nbplace + " personne(s).\nPrix : " + prix + "\nConducteur : " + nom_prenom + "\nCommentaire : " + commentaire);


                    }
                }


            } catch (Exception e) {
                Button button = findViewById(R.id.buttonfind);
                button.setVisibility(View.INVISIBLE);
                TextView tv = findViewById(R.id.textView);
                tv.setText("Aucun trajet ne correspond à votre demande");

            }
        }
        else{ // Si il n'y en a pas on enlève le bouton et on affiche un texte indiquant que la recherche n'a pas pu aboutir
            Button button = findViewById(R.id.buttonfind);
            button.setVisibility(View.INVISIBLE);
            TextView tv = findViewById(R.id.textView);
            tv.setText("Aucun trajet ne correspond à votre demande");


        }





    }


    public void versReservation(View view) { // cette méthode permet de passer à l'activité "Reservation" qui récapitule le trajet
                                             // et permet de réserver si on le souhaite

        Button boutonClique = (Button) view;
        String string= (String) boutonClique.getText();
        //int place = (int) boutonClique.getTag();
        int place = num_trajets_selec.get(0); // on clique sur le trajet qui répond aux critères de la recherche


            Intent annonceVersReservation = new Intent();
            for (int j =0 ; j<trajets.size() ; j++) {
                //envoie du vecteur trajet pour le conserver
                Trajet sauvtrajet = trajets.get(j);
                annonceVersReservation.setClass(this, CreerTrajet.class);
                annonceVersReservation.putExtra("sauvegarde"+j, sauvtrajet);
            }

            //envoie du trajet qui a été sélectionné
            annonceVersReservation.setClass(this,ReservationActivity.class);
            annonceVersReservation.putExtra("num_trajet",place);
            startActivity(annonceVersReservation);


    }
}
