package com.example.maxence.covoiturageapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Vector;


public class ReservationActivity extends AppCompatActivity {

Vector<Trajet> trajets = new Vector<Trajet>() ;
Trajet trajetClick;
int num_trajet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation);

        // reception du trajet cliqué :
        Intent recuperationTrajets = getIntent();
        trajets.remove(Enregistrement.chargerVecteurTrajet(trajets,recuperationTrajets)-1);
        num_trajet = recuperationTrajets.getIntExtra("num_trajet",0);
        trajetClick = trajets.get(num_trajet);

        /////////////////////////////////

        //On affiche tous ses attributs dans les TextViews créés dans le XML

        TextView tvdepart = findViewById(R.id.depart) ;
        tvdepart.setText( "Lieu de départ : "+ trajetClick.getDepart() ) ;

        TextView tvarrive = findViewById(R.id.arrive) ;
        tvarrive.setText( "Lieu d'arrivée : "+ trajetClick.getArrivee() ) ;

        TextView tvhoraire = findViewById(R.id.horaire) ;
        tvhoraire.setText("Heure des départs : " + trajetClick.getHeure() + "h" +  trajetClick.getMinutes() ) ;

        TextView tvjour = findViewById(R.id.jour) ;
        tvjour.setText( "Jour(s) du trajet : " + trajetClick.getJour() ) ;

        TextView tvprix = findViewById(R.id.prix) ;
        tvprix.setText( "Prix : " + trajetClick.getPrix() ) ;

        TextView tvNbPlaces = findViewById(R.id.nbplaces) ;
        tvNbPlaces.setText( "Il reste  " + trajetClick.getNbplaces() + " places" ) ;

        TextView tvNomConducteur = findViewById(R.id.nomConducteur) ;
        tvNomConducteur.setText( "Conducteur : " + trajetClick.getNomPrenom() ) ;

        TextView tvTelConducteur = findViewById(R.id.numeroConducteur) ;
        tvTelConducteur.setText( "Tel du conducteur : " + trajetClick.getNumeroTel() ) ;

        TextView tvCommentaire = findViewById(R.id.commentaire) ;
        tvCommentaire.setText( "Mot du conducteur : " + trajetClick.getCommentaire() ) ;

        // Le nouveau passager rentre son nom pour être ajouté au covoit et clique sur le bouton "réserver par message"
    }

    public void ConfirmerReservation(View view) {

        // on récupère le nom du nouveau passager

        EditText et = findViewById(R.id.nomNouveauPassager);
        String nvPassager = et.getText().toString() ;

        // envoi du message : (on laisse choisir entre les différentes apps SMS et Whatsapp)

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            //sendIntent.setData(Uri.parse("smsto:")) ; // pour n'utiliser que les applications SMS
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Bonjour ! Je m'appelle "+ nvPassager +" et je souhaite participer à votre trajet de "+trajetClick.getDepart() + " à " + trajetClick.getArrivee() +" le " +trajetClick.getJour() + " à " +  trajetClick.getHeure() + "h" +  trajetClick.getMinutes() );
            sendIntent.setType("text/plain");
            String phoneNumb = "33"+trajetClick.getNumeroTel().substring(1) ; //format du numéro de tel whatsapp
            sendIntent.putExtra("jid", phoneNumb +"@s.whatsapp.net"); // on envoie au conducteur si Whatsapp est choisie
            sendIntent.putExtra("address", trajetClick.getNumeroTel()); //pour les autres app SMS
            Intent choixAppMessage = Intent.createChooser(sendIntent, "Avec quelle application souhaitez vous contacter le conduteur ?");
            startActivity(choixAppMessage);

        //On décrémente le nb de places disponibles et on sauvegarde :
        trajetClick.decrementerNbPlacesDispo();
        sauvegarde() ;
    }

    private void sauvegarde() {
        SharedPreferences enreg = getSharedPreferences(String.valueOf(num_trajet), MODE_PRIVATE);
        SharedPreferences.Editor ed = enreg.edit();
        ed.putInt("nbplace",trajetClick.getNbplaces());
        ed.apply();
    }

    // Changement d'activité :
    public void changerActe(View view) {

        // on relaie le vecteur des trajets (MODIFIE !) :
        Intent trajetsVersPremiereActivity = new Intent();
        for (int j =0 ; j<trajets.size() ; j++) {
            Trajet sauvtrajet = trajets.get(j);
            trajetsVersPremiereActivity.setClass(this, RechercheActivity.class);
            trajetsVersPremiereActivity.putExtra("sauvegarde"+j, sauvtrajet);
        }
        startActivity(trajetsVersPremiereActivity);
    }

    // pour ajouter le covoit au Calendrier :
    public void addEvent(View view) {
        Intent intent = new Intent(Intent.ACTION_INSERT) ;
        intent.setData(CalendarContract.Events.CONTENT_URI) ;
        intent.putExtra(CalendarContract.Events.TITLE, "Trajet Eco'Covoit") ;
        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, trajetClick.getDepart()) ;
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, trajetClick.getHeure() + "h" +  trajetClick.getMinutes()) ;

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
        else {
            Toast.makeText(this, "Erreur avec le calendrier",Toast.LENGTH_SHORT).show() ;
        }

    }
}
