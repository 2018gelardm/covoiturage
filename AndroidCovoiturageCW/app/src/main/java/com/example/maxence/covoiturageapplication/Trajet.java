package com.example.maxence.covoiturageapplication;

import java.io.Serializable;
import java.util.Vector;

public class Trajet implements Serializable { // déclaration de la classe des trajets

    private String nom_prenom;
    private int prix;
    private String numeroTel;
    private String depart;
    private String arrivee;
    private String jourDeLaSemaine;
    private int heure;
    private int minutes;
    private int nbplaces;
    private String commentaire;
    private boolean existant;//variable permettant de définir si le trajet est à affiché ou non (si il a été supprimé par exemple)

    private Vector<Passager> passagers;//permet de définir les passager, sera utile lors de la version avec ajout de compte

    //surcharge de la fonction de création pour initialiser la valeur de existant à true

    public Trajet(String nom_prenom, String depart, String arrivee, int prix, String jourDeLaSemaine, int heure, int minutes, int nbplaces, String commentaire, String numeroTel, boolean existance) {
        this.nom_prenom = nom_prenom;
        this.prix = prix;
        this.jourDeLaSemaine = jourDeLaSemaine;
        this.heure = heure;
        this.minutes = minutes;
        this.commentaire = commentaire;
        this.nbplaces = nbplaces;
        this.numeroTel = numeroTel;
        this.depart = depart;
        this.arrivee = arrivee;
        this.existant = existance;
        this.verificationExistance();
    }

    public Trajet(String nom_prenom, String depart, String arrivee, int prix, String jourDeLaSemaine, int heure, int minutes, int nbplaces, String commentaire, String numeroTel) {
        this.nom_prenom = nom_prenom;
        this.prix = prix;
        this.jourDeLaSemaine = jourDeLaSemaine;
        this.heure = heure;
        this.minutes = minutes;
        this.commentaire = commentaire;
        this.nbplaces = nbplaces;
        this.numeroTel = numeroTel;
        this.depart = depart;
        this.arrivee = arrivee;
        this.existant = true;
        this.verificationExistance();
    }
    // Définition des accessseurs
    public String getNomPrenom(){
        return nom_prenom;
    }

    public int getPrix(){
        return prix;
    }

    public String getJour(){ return jourDeLaSemaine;    }

    public int getHeure(){
        return heure;
    }

    public int getMinutes(){
        return minutes;
    }

    public int getNbplaces(){
        return nbplaces;
    }

    public String getCommentaire(){ return commentaire; }

    public String getDepart(){return depart;}

    public String getArrivee(){return arrivee;}

    public String getNumeroTel() {return numeroTel;}

    public boolean getExistant() {return existant;}

    public void decrementerNbPlacesDispo() { // lorsqu'on réserve un trajet, permet de décrémenter le nombre de place
        nbplaces -- ;
        this.verificationExistance();
    }

    public void reservation(Passager passager){ // méthode permettant de réserver un trajet dans l'activité Réservation
        nbplaces --;
        passagers.add(passager);
        this.verificationExistance();
    }

    public void delete(){
        this.existant = false;
    }

    public void verificationExistance(){ // si un trajet n'a plus de place, on dit virtuellement qu'il n'existe plus (existant = false)
        if(this.nbplaces == 0){         // pour ensuite ne plus l'afficher
            this.existant = false;
        }
    }








}
