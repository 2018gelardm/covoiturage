package com.example.maxence.covoiturageapplication;

import android.app.Activity;
import android.content.Intent;

import java.util.Vector;

public class Enregistrement {

    public static int chargerVecteurTrajet(Vector<Trajet> trajets, Intent intent){ // méthode permettant de charger le vecteur trajet
        // depuis les activités précédentes
        boolean sortie = false;
        int j = 0;
        while (sortie == false){
            try{

                Trajet recuptrajet = (Trajet) intent.getSerializableExtra("sauvegarde"+j); // tant qu'il y a des trajets dans les préférences
                if (recuptrajet == null){                                                       // on les charge
                    sortie = true;
                }
                j++;

                trajets.add(recuptrajet);

            } catch (Exception e){
                sortie = true;
            }

        }
        return j;




    }

    public static void chargerVecteurTrajetv(Vector<Trajet> trajets,Intent intent){ // surcharge de cette méthode pour les ativités où il n'y a pas de risque de trajet "null" dans le vecteur des trajets
        boolean sortie = false;
        int j = 0;
        while (sortie == false){

            Trajet recuptrajet = (Trajet) intent.getSerializableExtra("sauvegarde" + j);
            trajets.add(recuptrajet);

            if (recuptrajet == null) {
                sortie = true;
                trajets.remove(j);

            }
            j++;

        }

    }



}
