package com.example.maxence.covoiturageapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Vector;

public class CreerTrajet extends AppCompatActivity implements View.OnClickListener {

    Vector<Trajet> trajets = new Vector<Trajet>(); // comme pour chaque activité, le vecteur de trajets est en variable globale pour l'utiliser dans les différentes méthodes
    EditText etnom; // Déclaration des différents champs de textes pour a création du trajret
    EditText etdepart;
    EditText etarrivee;
    EditText etprix;
    EditText ettelephone;
    EditText etnbplaces;
    EditText etcommentaire;
    EditText etheure;
    EditText etminute;
    EditText etjour;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creer_trajet);
        LinearLayout ll = findViewById(R.id.scrolllayout); // Déclaration du linear layout pour pouvoir le modifier dans le code java


        Intent recuperationTrajets = getIntent(); // on récupère le vecteur trajets depuis l'activité précédentes
        trajets.remove(Enregistrement.chargerVecteurTrajet(trajets,recuperationTrajets)-1); // méthode commune à toutes les activités pour importer ce vecteur




        etnom = new EditText(this);
        etnom.setHint("Votre nom");
        ll.addView(etnom);

        etdepart = new EditText(this);
        etdepart.setHint("Adresse départ");
        ll.addView(etdepart);

        etarrivee = new EditText(this);
        etarrivee.setHint("Adresse arrivée");
        ll.addView(etarrivee);

        etprix = new EditText(this);
        etprix.setHint("Prix du trajet");
        ll.addView(etprix);

        ettelephone = new EditText(this);
        ettelephone.setHint("Numéro de téléphone du conducteur");
        ll.addView(ettelephone);

        etnbplaces = new EditText(this);
        etnbplaces.setHint("Nombre de places");
        ll.addView(etnbplaces);

        etcommentaire = new EditText(this);
        etcommentaire.setHint("Commentaires");
        ll.addView(etcommentaire);

        etheure = new EditText(this);
        etheure.setHint("Heure");
        ll.addView(etheure);

        etminute = new EditText(this);
        etminute.setHint("Minute");
        ll.addView(etminute);

        etjour = new EditText(this);
        etjour.setHint("Jour de la semaine");
        ll.addView(etjour);

        Button buttonenregistrer = new Button(this);
        ll.addView(buttonenregistrer);
        buttonenregistrer.setText("Enregistrer ce trajet");
        buttonenregistrer.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {


        Trajet trajet = new Trajet(etnom.getText().toString(), etdepart.getText().toString (), etarrivee.getText().toString() ,(int) Integer.parseInt(etprix.getText().toString()), etjour.getText().toString(), (int) Integer.parseInt(etheure.getText().toString()), (int) Integer.parseInt(etminute.getText().toString()), (int) Integer.parseInt(etnbplaces.getText().toString()), etcommentaire.getText().toString(), ettelephone.getText().toString());
        trajets.add(trajet);

        //enregistrement du 1er trajet grace à la classe SharedPreferences
        SharedPreferences enreg = getSharedPreferences(String.valueOf(trajets.size()-1), MODE_PRIVATE); // l'id du xml correspond à l'index du trajet dans le vecteur trajets
        SharedPreferences.Editor ed = enreg.edit();
        ed.putString("nom_prenom",trajet.getNomPrenom());
        ed.putString("depart",trajet.getDepart());
        ed.putString("arrivee",trajet.getArrivee());
        ed.putInt("prix",trajet.getPrix());
        ed.putString("jour",trajet.getJour());
        ed.putInt("heure",trajet.getHeure());
        ed.putInt("minute",trajet.getMinutes());
        ed.putInt("nbplace",trajet.getNbplaces());
        ed.putString("commentaire",trajet.getCommentaire());
        ed.putString("num",trajet.getNumeroTel());
        ed.apply();
        



        Intent trajetsVersPremiereActivity = new Intent();


        for (int j =0 ; j<trajets.size() ; j++) { // boucle pour faire passer le vecteur de trajet à l'activité suivante

            Trajet sauvtrajet = trajets.get(j);
            trajetsVersPremiereActivity.setClass(this, RechercheActivity.class);
            trajetsVersPremiereActivity.putExtra("sauvegarde"+j, sauvtrajet); // on doit faire passer les trajets un à un car sinon la serialisation ne passe pas

        }
        startActivity(trajetsVersPremiereActivity);






    }

}

