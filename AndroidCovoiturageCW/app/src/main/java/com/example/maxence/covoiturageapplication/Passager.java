package com.example.maxence.covoiturageapplication;

class Passager {

    private String nom_prenom;
    private String numeroTel;

    public Passager(String nom_prenom, String numeroTel){
        this.nom_prenom = nom_prenom;
        this.numeroTel = numeroTel;
    }

    public String getNomPrenom(){return nom_prenom;}

    public String getNumeroTel(){return numeroTel;}
}
